<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateclippingVideosTable extends Migration
{
    public function up()
    {
        Schema::create('clipping_videos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clipping_id')->unsigned();
            $table->foreign('clipping_id')->references('id')->on('clipping')->onDelete('cascade');
            $table->string('link_video');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('clipping_videos');
    }
}
