<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateClippingGaleriasTable extends Migration
{
    public function up()
    {
        Schema::create('clipping_galerias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->integer('clipping_id')->unsigned();
            $table->foreign('clipping_id')->references('id')->on('clipping')->onDelete('cascade');
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('clipping_galerias');
    }
}
