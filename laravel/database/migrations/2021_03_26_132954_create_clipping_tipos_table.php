<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateClippingTiposTable extends Migration
{
    public function up()
    {
        Schema::create('clipping_tipos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('clipping_tipos');
    }
}
