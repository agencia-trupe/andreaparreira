@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Clipping | Link Externo |</small> Adicionar Link</h2>
</legend>

{!! Form::model($clipping, [
'route' => ['painel.clippings.link.store', $clipping->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.clippings.link.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection