@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Clipping | Link Externo |</small> Editar Galeria</h2>
</legend>

{!! Form::model($link, [
'route' => ['painel.clippings.link.update', $clipping->id, $link->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.clippings.link.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection