@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Clipping | Vídeo (YouTube)
    </h2>
    <a href="{{ route('painel.clippings.index') }}" title="Voltar para Mídias" class="btn btn-sm btn-default">
        &larr; Voltar para Clippings</a>
</legend>

@if($video == null)
<div class="alert alert-warning" role="alert" style="display: flex; justify-content: space-between; align-items: center;">Nenhum registro encontrado.
    <a href="{{ route('painel.clippings.video.create', $clipping->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Vídeo (YouTube)</a>
</div>

@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="clipping_videos">
    <thead>
        <tr>
            <th>Título</th>
            <th>Link Vídeo YouTube</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        <tr class="tr-row" id="{{ $video->id }}">
            <td>{{ $clipping->titulo }}</td>
            <td>{{ $video->link_video }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.clippings.video.destroy', $clipping->id, $video->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.clippings.video.edit', [$clipping->id, $video->id] ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
    </tbody>
</table>
@endif

@endsection