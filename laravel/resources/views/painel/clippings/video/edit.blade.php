@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Clipping | Vídeo (YouTube) |</small> Editar Vídeo</h2>
</legend>

{!! Form::model($video, [
'route' => ['painel.clippings.video.update', $clipping->id, $video->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.clippings.video.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection