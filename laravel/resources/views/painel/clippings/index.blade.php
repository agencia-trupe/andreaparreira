@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Clippings
        <a href="{{ route('painel.clippings.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Mídia</a>
    </h2>
</legend>

@if(!count($clippings))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="clipping">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Capa</th>
            <th>Título</th>
            <th>Gerenciar</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($clippings as $clipping)
        <tr class="tr-row" id="{{ $clipping->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>

            @if($clipping->tipo_id == 1)
            @foreach($capasImagens as $capa)
            @if($clipping->id == $capa->clipping_id)
            <td>
                <img src="{{ asset('assets/img/clippings/imagens/thumbs/'.$capa->imagem) }}" style="width: 100%; max-width:60px;" alt="">
                @break
            </td>
            @endif
            @endforeach

            @elseif($clipping->tipo_id == 2)
            @foreach($capasLinks as $capa)
            @if($clipping->id == $capa->clipping_id)
            <td>
                <img src="{{ asset('assets/img/clippings/link/'.$capa->capa) }}" style="width: 100%; max-width:60px;" alt="">
            </td>
            @endif
            @endforeach

            @elseif($clipping->tipo_id == 3)
            @foreach($capasVideos as $capa)
            @if($clipping->id == $capa->clipping_id)
            <td>
                <div class="video"><iframe style="width: 100%; max-width:100px; max-height:60px;" src="{{ $linkVideo.$capa->link_video }}"></iframe></div>
            </td>
            @endif
            @endforeach

            @else
            <td></td>
            @endif

            <td>{{ $clipping->titulo }}</td>
            <!-- <td>{{ $clipping->ano }}</td> -->

            <td>
                @foreach($tipos as $tipo)
                @if($tipo->id == $clipping->tipo_id && $tipo->id == 1)
                <a href="{{ route('painel.clippings.imagens.index', $clipping->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>{{ $tipo->titulo }}
                </a>
                @endif
                @if($tipo->id == $clipping->tipo_id && $tipo->id == 2)
                <a href="{{ route('painel.clippings.link.index', $clipping->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>{{ $tipo->titulo }}
                </a>
                @endif
                @if($tipo->id == $clipping->tipo_id && $tipo->id == 3)
                <a href="{{ route('painel.clippings.video.index', $clipping->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>{{ $tipo->titulo }}
                </a>
                @endif
                @endforeach
            </td>

            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.clippings.destroy', $clipping->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.clippings.edit', $clipping->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection