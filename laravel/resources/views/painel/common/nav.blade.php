<ul class="nav navbar-nav">
    <!-- <li class="dropdown @if(Tools::routeIs([
        'painel.home*',
        'painel.imagens*',
        'painel.destaques*',
        ])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Home
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.home*')) class="active" @endif>
                <a href="{{ route('painel.home.index') }}">Home</a>
            </li>
            <li @if(Tools::routeIs('painel.imagens*')) class="active" @endif>
                <a href="{{ route('painel.imagens.index') }}">Imagens</a>
            </li>
            <li @if(Tools::routeIs('painel.destaques*')) class="active" @endif>
                <a href="{{ route('painel.destaques.index') }}">Destaques</a>
            </li>
        </ul>
    </li> -->
    <li @if(Tools::routeIs('painel.banners*')) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>
    <li @if(Tools::routeIs('painel.perfil*')) class="active" @endif>
        <a href="{{ route('painel.perfil.index') }}">Perfil</a>
    </li>
    <li @if(Tools::routeIs('painel.partners*')) class="active" @endif>
        <a href="{{ route('painel.partners.index') }}">Parceiros</a>
    </li>
    <li @if(Tools::routeIs('painel.servicos*')) class="active" @endif>
        <a href="{{ route('painel.servicos.index') }}">Serviços</a>
    </li>
	<li @if(Tools::routeIs('painel.projetos*')) class="active" @endif>
		<a href="{{ route('painel.projetos.index') }}">Portfólio</a>
	</li>
    <li @if(Tools::routeIs('painel.clippings*')) class="active" @endif>
		<a href="{{ route('painel.clippings.index') }}">Clippings</a>
	</li>
    <li class="dropdown @if(Tools::routeIs('painel.contato*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>
</ul>
