@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('video', 'URL de incorporação do Vídeo (exemplo: https://www.youtube.com/embed/dvAbtpCxbCE)') !!}
    {!! Form::text('video', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.projetos.videos.index', $projeto->id) }}" class="btn btn-default btn-voltar">Voltar</a>
