@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem_1', 'Imagem de Perfil') !!}
    @if($registro->imagem_1)
    <img src="{{ url('assets/img/sobre/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_1', 'Título do Texto') !!}
    {!! Form::textarea('texto_1', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<hr>


<div class="form-group">
    {!! Form::label('texto_2', 'Texto de Perfil') !!}
    {!! Form::textarea('texto_2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<hr>


<div class="form-group">
    {!! Form::label('texto_3', 'Título do Segundo Texto') !!}
    {!! Form::textarea('texto_3', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('texto_4', 'Segundo Texto do Perfil') !!}
    {!! Form::textarea('texto_4', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
