import axios from 'axios';

import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('.banners').cycle({
    slides: '>.slide',
});

$('.scroll-down').click(event => {
    event.preventDefault();

    $('html, body').animate(
        {
            scrollTop: $('.banners').offset().top + $('.banners').height(),
        },
        1000
    );
});

window.Rellax('.rellax');

function getInstagramImages() {
    const url = 'https://www.instagram.com/deborahroig8/?__a=1';

    axios.get(url).then(({ data }) => {
        try {
            const { edges } = data.graphql.user.edge_owner_to_timeline_media;

            const pictures = edges.slice(0, 6).map(({ node }) => ({
                url: `https://instagram.com/p/${node.shortcode}`,
                thumbnail: node.thumbnail_resources[4].src,
            }));

            const $picturesDiv = $(`<div class="pictures"></div>`).append(
                pictures.map(
                    p =>
                        `<a href="${p.url}" target="_blank"><img src="${p.thumbnail}" /></a>`
                )
            );

            $('.home .instagram .center').append($picturesDiv);
        } catch (err) {
            console.error(err);
        }
    });
}

if ($('.home .instagram').length) {
    getInstagramImages();
}


 // FANCYBOX MIDIAS GALERIAS
 $(".fancybox, .projetos-show .pshow-imagembox a").fancybox({
  padding: 0,
  prevEffect: "fade",
  nextEffect: "fade",
  closeBtn: false,
  openEffect: "elastic",
  openSpeed: "150",
  closeEffect: "elastic",
  closeSpeed: "150",
  keyboard: true,
  helpers: {
    title: {
      type: "outside",
      position: "top",
    },
    overlay: {
      css: {
        background: "rgba(132, 134, 136, .88)",
      },
    },
  },
  mobile: {
    clickOutside: "close",
  },
  fitToView: false,
  autoSize: false,
  beforeShow: function () {
    this.maxWidth = "100%";
    this.maxHeight = "100%";
  },
});

$(".midia-galeria").click(function handler(e) {
  e.preventDefault();

  const id = $(this).data("galeria-id");

  if (id) {
    $(`a[rel=galeria-${id}]`)[0].click();
  }
});



function positionCategorias() {
    const $wrapper = $('.categorias');
    const $active = $wrapper.find('.active');

    const translate =
        $active.width() / 2 + $active.offset().left - $wrapper.offset().left;

    $wrapper.css('transform', `translateX(-${translate}px)`);
}

if ($('.categorias').length) {
    positionCategorias();
    setInterval(positionCategorias, 200);
}


  // PROJETOS/IMAGENS - BTN SCROLL TOP
  $(document).scroll(function (e) {
    e.preventDefault();
    $(".link-topo").toggle($(document).scrollTop() !== 0);
  });
  $(".link-topo").click(function (e) {
    e.preventDefault();
    $("html, body").animate({ scrollTop: $("body").offset().top }, 500);
});




  // AVISO DE COOKIES
$(document).ready(function() {
  
  $(".aviso-cookies").hide();

  if (window.location.href == routeHome) {
    $(".aviso-cookies").show();

    $(".aceitar-cookies").click(function () {
      var url = window.location.origin + window.location.pathname + "/aceite-de-cookies";

      $.ajax({
        type: "POST",
        url: url,
        success: function (data, textStatus, jqXHR) {
          $(".aviso-cookies").hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR, textStatus, errorThrown);
        },
      });
    });
  }
});