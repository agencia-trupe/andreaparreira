<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ClippingImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Clipping;
use App\Models\ClippingImagem;

use App\Helpers\CropImage;

class ClippingsImagensController extends Controller
{
    public function index(Clipping $registro)
    {
        $imagens = ClippingImagem::Clipping($registro->id)->ordenados()->get();

        return view('painel.clippings.imagens.index', compact('imagens', 'registro'));
    }

    public function show(Clipping $registro, ClippingImagem $imagem)
    {
        return $imagem;
    }

    public function store(Clipping $registro, ClippingImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = ClippingImagem::uploadImagem();
            $input['clipping_id'] = $registro->id;

            $imagem = ClippingImagem::create($input);

            $view = view('painel.clippings.imagens.imagem', compact('registro', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Clipping $registro, ClippingImagem $imagens_clippings)
    {
        
        try {
            $imagens_clippings->delete();
            return redirect()->route('painel.clippings.imagens.index', $registro->id)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Clipping $registro)
    {
        try {

            $registro->imagens()->delete();
            return redirect()->route('painel.clippings.imagens.index', $registro->id)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
