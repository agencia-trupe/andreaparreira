<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MidiaImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Midia;
use App\Models\MidiaImagem;

use App\Helpers\CropImage;

class MidiaImagensController extends Controller
{
    public function index(Midia $registro)
    {
        $imagens = MidiaImagem::midia($registro->id)->ordenados()->get();

        return view('painel.midia.imagens.index', compact('imagens', 'registro'));
    }

    public function show(Midia $registro, MidiaImagem $imagem)
    {
        return $imagem;
    }

    public function store(Midia $registro, MidiaImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = MidiaImagem::uploadImagem();
            $input['midia_id'] = $registro->id;

            $imagem = MidiaImagem::create($input);

            $view = view('painel.midia.imagens.imagem', compact('registro', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Midia $registro, MidiaImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.midia.imagens.index', $registro)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Midia $registro)
    {
        try {

            $registro->imagens()->delete();
            return redirect()->route('painel.midia.imagens.index', $registro)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
