<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PartnersRequest;

use App\Http\Requests;
use App\Models\Partners;

class PartnersController extends Controller
{
    public function index()
    {
        $registros = Partners::ordenados()->get();

        return view('painel.partners.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.partners.create');
    }

    public function store(PartnersRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Partners::upload_imagem();

            Partners::create($input);

            return redirect()->route('painel.partners.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Partners $registro)
    {
        return view('painel.partners.edit', compact('registro'));
    }

    public function update(PartnersRequest $request, Partners $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Partners::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.partners.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Partners $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.partners.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
