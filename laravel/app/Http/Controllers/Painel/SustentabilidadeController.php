<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SustentabilidadeRequest;
use App\Http\Controllers\Controller;

use App\Models\Sustentabilidade;

class SustentabilidadeController extends Controller
{
    public function index()
    {
        $registros = Sustentabilidade::ordenados()->get();

        return view('painel.sustentabilidade.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.sustentabilidade.create');
    }

    public function store(SustentabilidadeRequest $request)
    {
        try {

            $input = $request->all();

            Sustentabilidade::create($input);

            return redirect()->route('painel.sustentabilidade.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Sustentabilidade $registro)
    {
        return view('painel.sustentabilidade.edit', compact('registro'));
    }

    public function update(SustentabilidadeRequest $request, Sustentabilidade $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.sustentabilidade.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Sustentabilidade $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.sustentabilidade.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
