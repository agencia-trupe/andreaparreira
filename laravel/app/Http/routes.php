<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('projetos/{categoria_slug?}', 'ProjetosController@index')->name('projetos');
    Route::get('projetos/{categoria_slug}/{projeto_slug}', 'ProjetosController@show')->name('projetos.show');
    Route::get('clippings', 'ClippingController@index')->name('clippings');
    Route::get('perfil', 'SobreController@index')->name('perfil');
    Route::get('servicos', 'ServicosController@index')->name('servicos');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::get('politica-de-privacidade', 'PoliticaDePrivacidadeController@index')->name('politica-de-privacidade');
    Route::post('aceite-de-cookies', 'HomeController@postCookies')->name('aceite-de-cookies.post');



    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('destaques', 'DestaquesController');
		Route::resource('imagens', 'ImagensController');
		Route::resource('home', 'HomeController', ['only' => ['index', 'update']]);
        Route::resource('clippings', 'ClippingsController');
        Route::get('clippings/{clippings}/imagens/clear', [
            'as'   => 'painel.clippings.imagens.clear',
            'uses' => 'ClippingsImagensController@clear'
        ]);
        Route::resource('clippings.imagens', 'ClippingsImagensController', ['parameters' => ['imagens' => 'imagens_clippings']]);
        Route::resource('clippings.link', 'ClippingsLinksController');
        Route::resource('clippings.video', 'ClippingsVideosController');
	
		Route::resource('projetos/categorias', 'ProjetosCategoriasController', ['parameters' => ['categorias' => 'categorias_projetos']]);
		Route::resource('projetos', 'ProjetosController');
		Route::get('projetos/{projetos}/imagens/clear', [
			'as'   => 'painel.projetos.imagens.clear',
			'uses' => 'ProjetosImagensController@clear'
		]);
		Route::resource('projetos.imagens', 'ProjetosImagensController', ['parameters' => ['imagens' => 'imagens_projetos']]);
		Route::resource('projetos.videos', 'ProjetosVideosController', ['parameters' => ['videos' => 'videos_projetos']]);
		Route::resource('banners', 'BannersController');
		Route::resource('perfil', 'SobreController', ['only' => ['index', 'update']]);
        Route::resource('partners', 'PartnersController');
        Route::resource('servicos', 'ServicosController', ['only' => ['index', 'update']]);
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);
        Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
        Route::get('aceite-de-cookies', 'AceiteDeCookiesController@index')->name('painel.aceite-de-cookies');

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
