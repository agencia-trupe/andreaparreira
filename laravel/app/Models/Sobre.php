<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Sobre extends Model
{
    protected $table = 'sobre';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 450,
            'height' => null,
            'path'   => 'assets/img/sobre/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 960,
            'height' => null,
            'path'   => 'assets/img/sobre/'
        ]);
    }

    public static function upload_imagem_3()
    {
        return CropImage::make('imagem_3', [
            'width'  => 1920,
            'height' => null,
            'path'   => 'assets/img/sobre/'
        ]);
    }

    public static function upload_imagem_4()
    {
        return CropImage::make('imagem_4', [
            'width'  => 630,
            'height' => null,
            'path'   => 'assets/img/sobre/'
        ]);
    }

}
