<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Home extends Model
{
    protected $table = 'home';

    protected $guarded = ['id'];

    public static function upload_imagem_sobre_1()
    {
        return CropImage::make('imagem_sobre_1', [
            'width'  => 340,
            'height' => 340,
            'path'   => 'assets/img/home/'
        ]);
    }

    public static function upload_imagem_sobre_2()
    {
        return CropImage::make('imagem_sobre_2', [
            'width'  => 340,
            'height' => 340,
            'path'   => 'assets/img/home/'
        ]);
    }

}
